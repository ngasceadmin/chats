package com.nmims.service.interfaces;

import com.nmims.bean.CoordinatorDetails;

	 
public interface CoordinatorChatsService {
	CoordinatorDetails updateCoordinator(CoordinatorDetails coordinatorDetails);
	
	CoordinatorDetails deleteCoordinator(CoordinatorDetails coordinatorDetails);
	
	CoordinatorDetails addCoordinator(CoordinatorDetails coordinatorDetails);
}
