package com.nmims.service.interfaces;

import java.util.ArrayList;

import com.nmims.bean.ChatsGroupMember;
import com.nmims.bean.CoordinatorDetails;
import com.nmims.bean.SubjectGroupBean;

public interface PortalChatsService {

	boolean checkIfChatGroupAlreadyCreated(String chat_group_name);

	String createGroup(String chat_group_name,int studnet_subject_config_id) throws Exception;

	void insertUsersAndGroupDetails(ArrayList<ChatsGroupMember> sucessfullUserCreationList,
			SubjectGroupBean subjectDetailBean, String quickBloxGroupId, String chat_group_name) throws Exception;

	String createRoom(String chat_group_name,String groupid) throws Exception;

	void saveRoomMembers(ArrayList<ChatsGroupMember> roomMembersList) throws Exception;

}
