package com.nmims.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nmims.bean.CoordinatorDetails;
import com.nmims.helper.ChatHelper;
import com.nmims.service.interfaces.CoordinatorChatsService;

@Service
public class CoordinatorChatsServiceImpl implements CoordinatorChatsService {
	
	@Autowired
	ChatHelper helper;
	
	@Override
	public CoordinatorDetails updateCoordinator(CoordinatorDetails coordinatorDetails) {
	 return helper.updateCoordinator(coordinatorDetails);
	}

	@Override
	public CoordinatorDetails deleteCoordinator(CoordinatorDetails coordinatorDetails) {
		return helper.deleteCoordinator(coordinatorDetails);
		
	}

	@Override
	public CoordinatorDetails addCoordinator(CoordinatorDetails coordinatorDetails) {
		return helper.addCoordinator(coordinatorDetails);
	}
	

}
