package com.nmims.service.implementation;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.nmims.bean.ChatsGroupMember;
import com.nmims.bean.CoordinatorDetails;
import com.nmims.bean.SubjectGroupBean;
import com.nmims.helper.PortalChatsHelper;
import com.nmims.service.interfaces.PortalChatsService;

@Service
public class PortalChatsServiceImpl implements PortalChatsService {
	@Autowired
	PortalChatsHelper helper;
	
	@Override
	public boolean checkIfChatGroupAlreadyCreated(String chat_group_name) {
		// TODO Auto-generated method stub
		
		return false;
	}

	@Override
	public String createGroup(String chat_group_name,int studnet_subject_config_id) throws Exception {
		// TODO Auto-generated method stub
		return helper.createGroup(chat_group_name,studnet_subject_config_id);
	}

	@Override
	public void insertUsersAndGroupDetails(ArrayList<ChatsGroupMember> membersList,
			SubjectGroupBean subjectDetailBean, String quickBloxGroupId, String chat_group_name) throws Exception {
		
		/*
		long quickBloxPrimaryId=quickBloxDao.insertQuickBloxGroups(groupId, chatGroupName,subjectDetailBean.getStudent_subject_config_id());
		int primaryKey=(int)quickBloxPrimaryId;
		quickBloxDao.insertUserDetails(quickBloxUserIdsList,sucessfullUserCreationList,subjectDetailBean,primaryKey);
		 */
		
		 helper.saveGroupMembers(membersList);
		
	}

	@Override
	public String createRoom(String chat_group_name,String groupId) throws Exception {
		// TODO Auto-generated method stub
		return helper.createRoom(chat_group_name, groupId);
	}

	@Override
	public void saveRoomMembers(ArrayList<ChatsGroupMember> roomMembersList) throws Exception {
		
		 helper.saveRoomMembers(roomMembersList);		
	}
	

}
