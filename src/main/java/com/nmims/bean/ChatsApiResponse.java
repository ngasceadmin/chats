package com.nmims.bean;

public class ChatsApiResponse {
	
	private CoordinatorDetails data;
	boolean success ;
	String message;
	int  statusCode;

	
	public CoordinatorDetails getData() {
		return data;
	}
	public void setData(CoordinatorDetails data) {
		this.data = data;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	@Override
	public String toString() {
		return "ChatsApiResponse [data=" + data + ", success=" + success + ", message=" + message + ", statusCode="
				+ statusCode + "]";
	}
	
	
	

}
