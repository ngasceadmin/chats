package com.nmims.bean;

public class ChatsGroupMember {
  
  private String groupId;
  private String userId;
  private String groupName;
  private String firstName;
  private String lastName;
  private String userImageUrl;
  private String type;
  
  private String roomId;
  private String roomName;
  private String skillName;
  
  
  
  
  
  
  
  public String getSkillName() {
	return skillName;
}
public void setSkillName(String skillName) {
	this.skillName = skillName;
}
public String getRoomId() {
	return roomId;
}
public void setRoomId(String roomId) {
	this.roomId = roomId;
}
public String getRoomName() {
	return roomName;
}
public void setRoomName(String roomName) {
	this.roomName = roomName;
}
public String getType() {
    return type;
  }
  public void setType(String type) {
    this.type = type;
  }
  public String getGroupId() {
    return groupId;
  }
  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }
  public String getUserId() {
    return userId;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getGroupName() {
    return groupName;
  }
  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }
  public String getFirstName() {
    return firstName;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  public String getLastName() {
    return lastName;
  }
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  public String getUserImageUrl() {
    return userImageUrl;
  }
  public void setUserImageUrl(String userImageUrl) {
    this.userImageUrl = userImageUrl;
  }
  
  
@Override
public String toString() {
	return "ChatsGroupMember [groupId=" + groupId + ", userId=" + userId + ", groupName=" + groupName + ", firstName="
			+ firstName + ", lastName=" + lastName + ", userImageUrl=" + userImageUrl + ", type=" + type + ", roomId="
			+ roomId + ", roomName=" + roomName + ", skillName=" + skillName + "]";
}
  
  
  
  
  
  
  
}
