package com.nmims.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CoordinatorDetails {
	private String skillName;
	private String coordinatorId;
	private int Timebound_subject_config_id;
	
	private String groupId;
	private String roomId; 
	
	private String coordinatorIdToBeUpdate;
	
	
	
	
	
	
	
	public String getCoordinatorIdToBeUpdate() {
		return coordinatorIdToBeUpdate;
	}
	public void setCoordinatorIdToBeUpdate(String coordinatorIdToBeUpdate) {
		this.coordinatorIdToBeUpdate = coordinatorIdToBeUpdate;
	}

	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public int getTimebound_subject_config_id() {
		return Timebound_subject_config_id;
	}
	public void setTimebound_subject_config_id(int timebound_subject_config_id) {
		Timebound_subject_config_id = timebound_subject_config_id;
	}
	public String getSkillName() {
		return skillName;
	}
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	public String getCoordinatorId() {
		return coordinatorId;
	}
	public void setCoordinatorId(String coordinatorId) {
		this.coordinatorId = coordinatorId;
	}
	@Override
	public String toString() {
		return "CoordinatorDetails [skillName=" + skillName + ", coordinatorId=" + coordinatorId
				+ ", Timebound_subject_config_id=" + Timebound_subject_config_id + ", groupId=" + groupId + ", roomId="
				+ roomId + ", coordinatorIdToBeUpdate=" + coordinatorIdToBeUpdate + "]";
	}
	
	
	
	

}
