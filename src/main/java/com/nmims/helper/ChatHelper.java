package com.nmims.helper;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.nmims.bean.ChatsApiResponse;
import com.nmims.bean.CoordinatorDetails;

@Component
public class ChatHelper {
	
	  @Value("${CHAT_BASE_URL}")
	  private String coordinatorChatBaseUrl;
	  
	Logger logger = LoggerFactory.getLogger(ChatHelper.class);

	public CoordinatorDetails updateCoordinator(CoordinatorDetails coordinatorDetails) {
		try {
			String updateCoordinatorUrl=coordinatorChatBaseUrl+"chatCoordinator/updateCoordinator";
			RestTemplate template=new RestTemplate();
			ResponseEntity<ChatsApiResponse> updatedResponse=  template.postForEntity(updateCoordinatorUrl, coordinatorDetails, ChatsApiResponse.class);
			logger.info("Update status in chats "+ updatedResponse);
		
			
			 if(updatedResponse.getStatusCodeValue()!=200 && !updatedResponse.getBody().isSuccess()){
				 logger.info("Exception in updating coordinator "+ updatedResponse.getBody());
				 return new CoordinatorDetails();
			 }
			 
			 logger.info("Coordinator Added :: "+ updatedResponse.getBody());

			 return updatedResponse.getBody().getData();
			
		} catch (Exception e) {
			logger.error("Exception : occured in updating coordinator" + ExceptionUtils.getFullStackTrace(e));
			return new CoordinatorDetails();
		}
		
	}
	
	public CoordinatorDetails deleteCoordinator(CoordinatorDetails coordinatorDetails) {
		try {
			logger.info("coordinatorDetails"+coordinatorDetails);
		String deleteCoordinatorUrl=coordinatorChatBaseUrl+"chatCoordinator/deleteCoordinator";
		RestTemplate template=new RestTemplate();
		 ResponseEntity<ChatsApiResponse> deletedResponse=   template.postForEntity(deleteCoordinatorUrl, coordinatorDetails, ChatsApiResponse.class);
			logger.info("deleteCoordinatorUrl"+deleteCoordinatorUrl);
			logger.info("Delete Status "+ deletedResponse);
			
			 if(deletedResponse.getStatusCodeValue()!=200 && !deletedResponse.getBody().isSuccess()){
				 logger.info("Exception in adding coordinator "+ deletedResponse.getBody());
				 return new CoordinatorDetails();
			 }
			 
			 logger.info("Coordinator Added :: "+ deletedResponse.getBody());

			 return deletedResponse.getBody().getData();

		}
		catch (Exception e) {
			logger.error("Exception : occured in deleting coordinator" + ExceptionUtils.getFullStackTrace(e));
			return new CoordinatorDetails();
		}
		
	}
	
	public CoordinatorDetails addCoordinator(CoordinatorDetails coordinatorDetails) {
		try {
			System.out.println("coordinatorDetails"+coordinatorDetails);
			String addCoordinatorUrl=coordinatorChatBaseUrl+"chatCoordinator/addCoordinator";
			System.out.println("addCoordinatorUrl "+ addCoordinatorUrl);
			RestTemplate template=new RestTemplate();
			 ResponseEntity<ChatsApiResponse> addedResponse=   template.postForEntity(addCoordinatorUrl, coordinatorDetails,ChatsApiResponse.class);
				
			 logger.info("adding coordinator "+ addedResponse.getBody());

			 if(addedResponse.getStatusCodeValue()!=200 && !addedResponse.getBody().isSuccess()){
				 return new CoordinatorDetails();
			 }
			 
			 logger.info("Coordinator Added :: "+ addedResponse.getBody().getData());

			 return addedResponse.getBody().getData();

		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("Exception : occured in adding" + ExceptionUtils.getFullStackTrace(e));
			return new CoordinatorDetails();
		}
	}
}
