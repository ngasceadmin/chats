package com.nmims.helper;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nmims.bean.ChatsGroupMember;
import com.nmims.bean.ChatsGroups;
import com.nmims.bean.CoordinatorDetails;
import com.nmims.dao.QuickBloxDao;

@Component
public class PortalChatsHelper {
  
	 @Value("${CHAT_BASE_URL}")
	  private String quickBloxBaseUrl; 
	
	@Autowired
	QuickBloxDao quickBloxDao;
	
	private static final Logger logger = LoggerFactory.getLogger(PortalChatsHelper.class);
	

	public HttpHeaders getHeaders()
	{
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		headers.add("Accept", "application/json");
		return headers;
	}
	
	
	public String createGroup(String groupName,int student_subject_config_id) throws Exception
	{
		try
		{
			String groupCreationUrl=quickBloxBaseUrl+"chatgroups/save";
			HttpHeaders headers = getHeaders();
			JsonObject requestBody = new JsonObject();
			requestBody.addProperty("name", groupName);
			HttpEntity<String> requestEntity = new HttpEntity<String>(requestBody.toString(),headers);
			RestTemplate restTemplate = new RestTemplate();
			long t1=System.currentTimeMillis();
			ResponseEntity<String> response=restTemplate.exchange(groupCreationUrl, 
					HttpMethod.POST,
					requestEntity,
					String.class);
			long t2=System.currentTimeMillis();
			long t3=t2-t1;
			logger.info("Response time from group creation api in milli seconds:"+t3);
			logger.info("Response body from group creation api: "+response.getBody());
			JsonObject json = new JsonParser().parse(response.getBody()).getAsJsonObject();
			if("200".equalsIgnoreCase(Integer.toString(response.getStatusCodeValue())))
			{
				String groupId=json.get("id").getAsString();
				logger.info("group id: "+groupId);
				System.out.println("in createGroup : groupId : "+groupId);
				try {
			long result=quickBloxDao.insertQuickBloxGroups(groupId,groupName,student_subject_config_id);
			
			if((!"".equals(result))) {
				logger.info("Suucessfully saved the data "+ result);
				
			}
				}
				catch(Exception e) {
					logger.info("error ocuured saving the data " + e.getMessage());
					e.printStackTrace();
				}
				 
				return groupId;
			}
			throw new Exception(response.getBody());
		}
		catch(HttpClientErrorException httpex)
		{
			String body=httpex.getResponseBodyAsString();
			String status=httpex.getStatusCode().toString();
			logger.info("HttpClientException in group creation: "+httpex);
			logger.info("body in group creation: "+body);
			logger.info("status in group creation: "+status);
			throw new Exception(body);
		}
		catch(Exception e)
		{
			logger.info("Exception in group creation: "+e);
			throw new Exception(e.getMessage());
		}
		
	}


	public void saveGroupMembers(ArrayList<ChatsGroupMember> membersList) throws Exception {
		try
		{
			String groupMembersCreationUrl=quickBloxBaseUrl+"chatgroupmember/saveList";
			logger.info("groupMembersCreationUrl:"+groupMembersCreationUrl);
			HttpHeaders headers = getHeaders();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(membersList,headers);
			RestTemplate restTemplate = new RestTemplate();
			long t1=System.currentTimeMillis();
			ResponseEntity<String> response=restTemplate.exchange(groupMembersCreationUrl, 
					HttpMethod.POST,
					requestEntity,
					String.class);
			long t2=System.currentTimeMillis();
			long t3=t2-t1;
			logger.info("Response time from groupmember creation api in milli seconds:"+t3);
			logger.info("Response body from groupmember creation api: "+response.getBody());
			JsonArray json = new JsonParser().parse(response.getBody()).getAsJsonArray();
			if("200".equalsIgnoreCase(Integer.toString(response.getStatusCodeValue())))
			{
				System.out.println("in createGroupMembers : created! ");
				return;
			}
			throw new Exception(response.getBody());
		}
		catch(HttpClientErrorException httpex)
		{
			String body=httpex.getResponseBodyAsString();
			String status=httpex.getStatusCode().toString();
			logger.info("HttpClientException in groupmember creation: "+httpex);
			logger.info("body in groupmember creation: "+body);
			logger.info("status in groupmember creation: "+status);
			throw new Exception(body);
		}
		catch(Exception e)
		{
			logger.info("Exception in groupmember creation: "+e);
			throw new Exception(e.getMessage());
		}
		
	}
//rooms start

	public String createRoom(String roomName,String groupid) throws Exception
	{
		try
		{
			String roomCreationUrl=quickBloxBaseUrl+"chatroom/save";
			logger.info("roomCreationUrl:"+roomCreationUrl);
			HttpHeaders headers = getHeaders();
			JsonObject requestBody = new JsonObject();
			requestBody.addProperty("name", roomName);
			requestBody.addProperty("roomUrl", "https://cdn-icons-png.flaticon.com/512/615/615075.png");
			HttpEntity<String> requestEntity = new HttpEntity<String>(requestBody.toString(),headers);
			RestTemplate restTemplate = new RestTemplate();
			long t1=System.currentTimeMillis();
			ResponseEntity<String> response=restTemplate.exchange(roomCreationUrl, 
					HttpMethod.POST,
					requestEntity,
					String.class);
			long t2=System.currentTimeMillis();
			long t3=t2-t1;
			logger.info("Response time from roomName creation api in milli seconds:"+t3);
			logger.info("Response body from roomName creation api: "+response.getBody());
			JsonObject json = new JsonParser().parse(response.getBody()).getAsJsonObject();
			if("200".equalsIgnoreCase(Integer.toString(response.getStatusCodeValue())))
			{
				String roomId=json.get("roomId").getAsString();
				logger.info("roomId id: "+roomId);
				System.out.println("in createRoom : roomId : "+roomId);
				quickBloxDao.updateQuickBloxGroups(roomId, groupid);
				return roomId;
			}
			throw new Exception(response.getBody());
		}
		catch(HttpClientErrorException httpex)
		{
			String body=httpex.getResponseBodyAsString();
			String status=httpex.getStatusCode().toString();
			logger.info("HttpClientException in roomId creation: "+httpex);
			logger.info("body in roomId creation: "+body);
			logger.info("status in roomId creation: "+status);
			throw new Exception(body);
		}
		catch(Exception e)
		{
			logger.info("Exception in roomId creation: "+e);
			throw new Exception(e.getMessage());
		}
		
	}


	public void saveRoomMembers(ArrayList<ChatsGroupMember> roomsList) throws Exception {
		try
		{
			String roomMembersCreationUrl=quickBloxBaseUrl+"chatroommember/saveList";
			logger.info("roomMembersCreationUrl:"+roomMembersCreationUrl);
			HttpHeaders headers = getHeaders();
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(roomsList,headers);
			RestTemplate restTemplate = new RestTemplate();
			long t1=System.currentTimeMillis();
			ResponseEntity<String> response=restTemplate.exchange(roomMembersCreationUrl, 
					HttpMethod.POST,
					requestEntity,
					String.class);
			long t2=System.currentTimeMillis();
			long t3=t2-t1;
			logger.info("Response time from roommember creation api in milli seconds:"+t3);
			logger.info("Response body from roommember creation api: "+response.getBody());
			JsonArray json = new JsonParser().parse(response.getBody()).getAsJsonArray();
			if("200".equalsIgnoreCase(Integer.toString(response.getStatusCodeValue())))
			{
				System.out.println("in createroomMembers : created! ");
				return;
			}
			throw new Exception(response.getBody());
		}
		catch(HttpClientErrorException httpex)
		{
			String body=httpex.getResponseBodyAsString();
			String status=httpex.getStatusCode().toString();
			logger.info("HttpClientException in roommember creation: "+httpex);
			logger.info("body in roommember creation: "+body);
			logger.info("status in roommember creation: "+status);
			throw new Exception(body);
		}
		catch(Exception e)
		{
			logger.info("Exception in roommember creation: "+e);
			throw new Exception(e.getMessage());
		}
		
	}
//rooms end
	
}
