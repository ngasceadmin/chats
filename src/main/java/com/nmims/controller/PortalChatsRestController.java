package com.nmims.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nmims.bean.ChatsGroupMember;
import com.nmims.bean.CoordinatorDetails;
import com.nmims.bean.StudentBean;
import com.nmims.bean.SubjectGroupBean;
import com.nmims.service.interfaces.PortalChatsService;
import com.nmims.services.QuickBloxService;

@RestController
@RequestMapping("/api/portalchats/")
public class PortalChatsRestController {
	
	@Autowired
	QuickBloxService quickBloxService;
	
	@Autowired
	PortalChatsService service;
	
	@Value("${CURRENT_MBAWX_ACAD_MONTH}")
	String acadMonth;
	@Value("${CURRENT_MBAWX_ACAD_YEAR}")
	String acadYear;
	
	private static final Logger logger = LoggerFactory.getLogger(PortalChatsRestController.class);

	private static final String CC_PROFILE_IMG = "https://staticfileschat.s3.ap-south-1.amazonaws.com/img/cc_profile_manager.png";

	@GetMapping("chatGroupCreation")
	public ResponseEntity<String> chatGroupCreation(){
		
		System.out.println("acadMonth"+ acadMonth);
		System.out.println("acadYear"+ acadYear );
	
		try {

			List<SubjectGroupBean> subjectDetailsList = new ArrayList<SubjectGroupBean>();
			
				subjectDetailsList = quickBloxService.getActiveSubjectsDetailsList();
				logger.info("active subject size is: "+subjectDetailsList.size());
				logger.info("subject list is " +subjectDetailsList);
			
			for(SubjectGroupBean subjectDetailBean : subjectDetailsList)
			{
				logger.info("subjectDetailsList"+subjectDetailBean.getStudent_subject_config_id());
				boolean hasCCUser = false;
				logger.info(subjectDetailBean.toString());
				subjectDetailBean.setSubject_initials(quickBloxService.getInitials(subjectDetailBean.getSubject()));
				String chat_group_name = quickBloxService.getChatGroupName( subjectDetailBean );
				LocalDateTime date = LocalDateTime.now();
				System.out.println("DateTime " +  date + "chat_group_name : "+chat_group_name);
				subjectDetailBean.setChat_group_name(chat_group_name);
				boolean groupAlreadyCretead=false;
				List<ChatsGroupMember> memberList = new ArrayList<ChatsGroupMember>();
				ArrayList<ChatsGroupMember> sucessfullUserCreationList = new ArrayList<ChatsGroupMember>();
				List<CoordinatorDetails> coordinatorList=new ArrayList<>();
				
				String courseCordinatorSapId=subjectDetailBean.getUserId();
				String courseCordinatorTumId=Integer.toString(subjectDetailBean.getTimebound_user_mapping_id());
				String courseCourdinatorQuickBloxId="";
				String quickBloxGroupId="";
				String roomId="";
				try
				{
					logger.info(subjectDetailBean.getChat_group_name());
					groupAlreadyCretead=quickBloxService.checkIfChatGroupAlreadyCreated(subjectDetailBean.getStudent_subject_config_id());
					//  groupAlreadyCretead=service.checkIfChatGroupAlreadyCreated(chat_group_name);
					logger.info(subjectDetailBean.getStudent_subject_config_id()+" group created: "+ groupAlreadyCretead);
				}
				catch(Exception e)
				{
					logger.info("Exception in checking if group already created for student subject config id: "+subjectDetailBean.getStudent_subject_config_id()+" is "+ExceptionUtils.getStackTrace(e));
					continue;
				}
				
				if(!groupAlreadyCretead)
				{
					try
					{
						quickBloxGroupId=service.createGroup(subjectDetailBean.getChat_group_name(),subjectDetailBean.getStudent_subject_config_id());
						roomId=service.createRoom(subjectDetailBean.getChat_group_name(),quickBloxGroupId);
						logger.info("got groupid n roomid : g "+quickBloxGroupId+" r "+roomId);
						
						if(StringUtils.isBlank(quickBloxGroupId) || StringUtils.isBlank(roomId) )
						{
							logger.info("got groupid or roomid blank : g "+quickBloxGroupId+" r "+roomId);
							continue;
						}
						memberList=quickBloxService.getGroupMembersForPortalChats(subjectDetailBean.getStudent_subject_config_id());
						coordinatorList=quickBloxService.getCoordinatorForSpecificSkills(subjectDetailBean.getStudent_subject_config_id());
						if(0!=coordinatorList.size()) {
							hasCCUser=true;
							ArrayList<ChatsGroupMember> coordinatorBeanList = new ArrayList<ChatsGroupMember>();
						for(CoordinatorDetails coordinator:coordinatorList) {
							ChatsGroupMember coordinatorMember=new ChatsGroupMember();
							coordinatorMember.setUserId(coordinator.getCoordinatorId());
							coordinatorMember.setFirstName("CC :");
							coordinatorMember.setLastName(coordinator.getCoordinatorId());
							coordinatorMember.setType("coursecoordinator");
							coordinatorMember.setUserImageUrl(CC_PROFILE_IMG);
							
							coordinatorMember.setGroupId(quickBloxGroupId);
							coordinatorMember.setGroupName(subjectDetailBean.getChat_group_name());
							coordinatorMember.setRoomId(roomId);
							coordinatorMember.setRoomName(subjectDetailBean.getChat_group_name());

							
							logger.info("Coordinator skills "+ coordinator.getSkillName());
							
							if(coordinator.getSkillName().trim().isEmpty() || coordinator.getSkillName()==null) {
								coordinatorMember.setSkillName("Admissions");
							}
							else {
								coordinatorMember.setSkillName(coordinator.getSkillName());
							}
			
							coordinatorBeanList.add(coordinatorMember);
						}
						
						service.insertUsersAndGroupDetails(coordinatorBeanList,subjectDetailBean,quickBloxGroupId,subjectDetailBean.getChat_group_name());
						service.saveRoomMembers(coordinatorBeanList);
						
						}
						
						for(ChatsGroupMember bean:memberList)
						{
							try
							{
								bean.setGroupId(quickBloxGroupId);
								bean.setGroupName(subjectDetailBean.getChat_group_name());
								bean.setRoomId(roomId);
								bean.setRoomName(subjectDetailBean.getChat_group_name());
								
								if("coursecoordinator".equalsIgnoreCase(bean.getType())) {
									bean.setUserImageUrl(CC_PROFILE_IMG);
								}
								
								sucessfullUserCreationList.add(bean);
								logger.info("user id:"+bean.getUserId()+" already exist in quickblox system for subject:"+subjectDetailBean.getStudent_subject_config_id());
								
							}
							catch(Exception e)
							{
								logger.info("Exception in checkIfUserExists for user id:"+bean.getUserId()+" for subject:"+subjectDetailBean.getStudent_subject_config_id()+" is:"+e.getMessage());
							}
						}
						
						if(!hasCCUser) {
							ChatsGroupMember ccUserBean = new ChatsGroupMember();
							ccUserBean.setType("coursecoordinator");
							ccUserBean.setGroupId(quickBloxGroupId);
							ccUserBean.setGroupName(subjectDetailBean.getChat_group_name());
							ccUserBean.setRoomId(roomId);
							ccUserBean.setRoomName(subjectDetailBean.getChat_group_name());

							ccUserBean.setUserImageUrl(CC_PROFILE_IMG);
							ccUserBean.setFirstName("CC :");
							ccUserBean.setLastName("Coordinator");
							ccUserBean.setUserId("courseCoordinator");
							ccUserBean.setSkillName("Admissions");
							
							ArrayList<ChatsGroupMember> ccUserList = new ArrayList<ChatsGroupMember>();
							ccUserList.add(ccUserBean);
							service.insertUsersAndGroupDetails(ccUserList,subjectDetailBean,quickBloxGroupId,subjectDetailBean.getChat_group_name());
							service.saveRoomMembers(ccUserList);
						
							
						}
						
						logger.info("The members in the group "+ memberList);
						
						service.insertUsersAndGroupDetails(sucessfullUserCreationList,subjectDetailBean,quickBloxGroupId,subjectDetailBean.getChat_group_name());
						service.saveRoomMembers(sucessfullUserCreationList);
					}
					catch(Exception e)
					{
						logger.info("Exception for student subject config id: "+subjectDetailBean.getStudent_subject_config_id()+" is "+ExceptionUtils.getStackTrace(e));
					}
					
				}
				else
				{
					/*
					try
					{
						memberList=quickBloxService.getNewGroupMembers(subjectDetailBean.getStudent_subject_config_id());
						if(memberList.size()>0)
						{
							for(ChatsGroupMember bean:memberList)
							{
								try
								{
									String quickBloxId=quickBloxService.checkIfUserExists(bean.getUserId(),token);
									if(!quickBloxId.equalsIgnoreCase("No user found"))
									{
										quickBloxUserIdsList.add(quickBloxId);
										sucessfullUserCreationList.add(bean);
										logger.info("user id:"+bean.getUserId()+" already exist in quickblox system for subject:"+subjectDetailBean.getStudent_subject_config_id());
									}
								}
								catch(Exception e)
								{
									logger.info("Exception in checkIfUserExists for user id:"+bean.getUserId()+" for subject:"+subjectDetailBean.getStudent_subject_config_id()+" is:"+e.getMessage());
								}
							}
							String userToken=quickBloxService.getUserSession(courseCordinatorSapId);
							quickBloxGroupId=quickBloxService.updateGroup(userToken,subjectDetailBean.getStudent_subject_config_id(), quickBloxUserIdsList);
							quickBloxService.insertUserDetails(quickBloxUserIdsList,sucessfullUserCreationList,subjectDetailBean,quickBloxGroupId);
						}
						else
						{
							logger.info("No new member available for student subject config id:"+subjectDetailBean.getStudent_subject_config_id());
						}
					}
					catch(Exception e)
					{
						logger.info("Exception for student subject config id: "+subjectDetailBean.getStudent_subject_config_id()+" is "+ExceptionUtils.getStackTrace(e));
					}
				*/
				}
			}		
			
		
			return new ResponseEntity<>("Sucess", HttpStatus.OK);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<>("Error : "+e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	
	}

	@RequestMapping(value="chatGroupCreation",method= {RequestMethod.POST})
	@ResponseBody
	public void chatGroupCreationold()
	{
		List<SubjectGroupBean> subjectDetailsList = new ArrayList<SubjectGroupBean>();
		String token="";
		try
		{
			subjectDetailsList = quickBloxService.getActiveSubjectsDetailsList();
			logger.info("active subject size is: "+subjectDetailsList.size());
			token=quickBloxService.getUserSession("77777777");
		}
		catch(Exception e)
		{
			logger.info("Exception in getting active subject details: "+ExceptionUtils.getStackTrace(e));
			return;
		}
		
		for(SubjectGroupBean subjectDetailBean : subjectDetailsList)
		{
			logger.info(subjectDetailBean.toString());
			subjectDetailBean.setSubject_initials(quickBloxService.getInitials(subjectDetailBean.getSubject()));
			String chat_group_name = quickBloxService.getChatGroupName( subjectDetailBean );
			subjectDetailBean.setChat_group_name(chat_group_name);
			boolean groupAlreadyCretead=false;
			List<StudentBean> memberList = new ArrayList<StudentBean>();
			ArrayList<StudentBean> sucessfullUserCreationList = new ArrayList<StudentBean>();
			ArrayList<String> quickBloxUserIdsList= new ArrayList<String>();
			String courseCordinatorSapId=subjectDetailBean.getUserId();
			String courseCordinatorTumId=Integer.toString(subjectDetailBean.getTimebound_user_mapping_id());
			String courseCourdinatorQuickBloxId="";
			String quickBloxGroupId="";
			try
			{
				logger.info(subjectDetailBean.getChat_group_name());
				groupAlreadyCretead=quickBloxService.checkIfChatGroupAlreadyCreated(subjectDetailBean.getStudent_subject_config_id());
				logger.info(subjectDetailBean.getStudent_subject_config_id()+" group created: "+ groupAlreadyCretead);
			}
			catch(Exception e)
			{
				logger.info("Exception in checking if group already created for student subject config id: "+subjectDetailBean.getStudent_subject_config_id()+" is "+ExceptionUtils.getStackTrace(e));
				continue;
			}
			
			if(!groupAlreadyCretead)
			{
				try
				{
					memberList=quickBloxService.getGroupMembers(subjectDetailBean.getStudent_subject_config_id());
					for(StudentBean bean:memberList)
					{
						try
						{
							String quickBloxId=quickBloxService.checkIfUserExists(bean.getUserId(),token);
							if(!quickBloxId.equalsIgnoreCase("No user found"))
							{
								quickBloxUserIdsList.add(quickBloxId);
								sucessfullUserCreationList.add(bean);
								logger.info("user id:"+bean.getUserId()+" already exist in quickblox system for subject:"+subjectDetailBean.getStudent_subject_config_id());
							}
						}
						catch(Exception e)
						{
							logger.info("Exception in checkIfUserExists for user id:"+bean.getUserId()+" for subject:"+subjectDetailBean.getStudent_subject_config_id()+" is:"+e.getMessage());
						}
					}
					
					courseCourdinatorQuickBloxId=quickBloxService.checkIfUserExists(courseCordinatorSapId,token);
					if(courseCourdinatorQuickBloxId.equalsIgnoreCase("No user found"))
					{
						courseCourdinatorQuickBloxId = quickBloxService.createUser(token, courseCordinatorSapId);
					}
					
					String userToken=quickBloxService.getUserSession(courseCordinatorSapId);
					quickBloxGroupId=quickBloxService.createGroup(userToken,subjectDetailBean.getChat_group_name(), quickBloxUserIdsList);
					quickBloxUserIdsList.add(courseCourdinatorQuickBloxId);
					StudentBean courseCordinatorBean = new StudentBean();
					courseCordinatorBean.setId(courseCordinatorTumId);
					courseCordinatorBean.setUserId(courseCordinatorSapId);
					sucessfullUserCreationList.add(courseCordinatorBean);
					quickBloxService.insertUsersAndGroupDetails(quickBloxUserIdsList,sucessfullUserCreationList,subjectDetailBean,quickBloxGroupId,subjectDetailBean.getChat_group_name());
				}
				catch(Exception e)
				{
					logger.info("Exception for student subject config id: "+subjectDetailBean.getStudent_subject_config_id()+" is "+ExceptionUtils.getStackTrace(e));
				}
				
			}
			else
			{
				try
				{
					memberList=quickBloxService.getNewGroupMembers(subjectDetailBean.getStudent_subject_config_id());
					if(memberList.size()>0)
					{
						for(StudentBean bean:memberList)
						{
							try
							{
								String quickBloxId=quickBloxService.checkIfUserExists(bean.getUserId(),token);
								if(!quickBloxId.equalsIgnoreCase("No user found"))
								{
									quickBloxUserIdsList.add(quickBloxId);
									sucessfullUserCreationList.add(bean);
									logger.info("user id:"+bean.getUserId()+" already exist in quickblox system for subject:"+subjectDetailBean.getStudent_subject_config_id());
								}
							}
							catch(Exception e)
							{
								logger.info("Exception in checkIfUserExists for user id:"+bean.getUserId()+" for subject:"+subjectDetailBean.getStudent_subject_config_id()+" is:"+e.getMessage());
							}
						}
						String userToken=quickBloxService.getUserSession(courseCordinatorSapId);
						quickBloxGroupId=quickBloxService.updateGroup(userToken,subjectDetailBean.getStudent_subject_config_id(), quickBloxUserIdsList);
						quickBloxService.insertUserDetails(quickBloxUserIdsList,sucessfullUserCreationList,subjectDetailBean,quickBloxGroupId);
					}
					else
					{
						logger.info("No new member available for student subject config id:"+subjectDetailBean.getStudent_subject_config_id());
					}
				}
				catch(Exception e)
				{
					logger.info("Exception for student subject config id: "+subjectDetailBean.getStudent_subject_config_id()+" is "+ExceptionUtils.getStackTrace(e));
				}
			}
		}		
		
	}
	
}
