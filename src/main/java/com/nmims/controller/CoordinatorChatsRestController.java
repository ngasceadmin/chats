package com.nmims.controller;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nmims.bean.ChatsApiResponse;
import com.nmims.bean.CoordinatorDetails;
import com.nmims.service.interfaces.CoordinatorChatsService;
import com.nmims.services.QuickBloxService;

@RequestMapping("/api/coordinatorchats/")
@RestController
public class CoordinatorChatsRestController {
	
	@Autowired
	QuickBloxService quickBloxService;
	
	@Autowired
	CoordinatorChatsService coordinatorChatsService;
	
	Logger logger = LoggerFactory.getLogger(CoordinatorChatsRestController.class);
	

	@PostMapping("updateCoordinatorAndSkills")
	public ResponseEntity<ChatsApiResponse> updateCoordinatorAndSkills(@RequestBody CoordinatorDetails coordinatorDetails) throws Exception{
		ChatsApiResponse updatedCoordinatorApiResponse = new ChatsApiResponse();

	try {
		 CoordinatorDetails coordinatorGroupAndRoomId =quickBloxService.getGroupId(coordinatorDetails.getTimebound_subject_config_id());
		coordinatorDetails.setGroupId(coordinatorGroupAndRoomId.getGroupId());
		coordinatorDetails.setRoomId(coordinatorGroupAndRoomId.getRoomId());
		System.out.println("CoordinatorDetails"+coordinatorDetails);
		CoordinatorDetails updatedCoordinator= coordinatorChatsService.updateCoordinator(coordinatorDetails);
		if(StringUtils.isBlank(updatedCoordinator.getGroupId())) {
			updatedCoordinatorApiResponse.setData(new CoordinatorDetails());
			updatedCoordinatorApiResponse.setMessage("Coordinator not updated  ! ");
			updatedCoordinatorApiResponse.setStatusCode(500);
			updatedCoordinatorApiResponse.setSuccess(false);
			return new ResponseEntity<ChatsApiResponse>(updatedCoordinatorApiResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		updatedCoordinatorApiResponse.setData(updatedCoordinator);
		updatedCoordinatorApiResponse.setMessage("Coordinator Updated Sucessfully! ");
		updatedCoordinatorApiResponse.setStatusCode(200);
		updatedCoordinatorApiResponse.setSuccess(true);
		return new ResponseEntity<ChatsApiResponse>(updatedCoordinatorApiResponse,HttpStatus.OK);
			}
			catch(Exception e) {
				logger.info("Exception in adding coordiantor"+ ExceptionUtils.getFullStackTrace(e));
				updatedCoordinatorApiResponse.setData(new CoordinatorDetails());
				updatedCoordinatorApiResponse.setMessage(e.getMessage());
				updatedCoordinatorApiResponse.setStatusCode(500);
				updatedCoordinatorApiResponse.setSuccess(false);
				return new ResponseEntity<ChatsApiResponse>(updatedCoordinatorApiResponse,HttpStatus.INTERNAL_SERVER_ERROR);	
			}		
		
	}
	@PostMapping("deleteCoordinator")
	public ResponseEntity<ChatsApiResponse> deleteCoordinatorAndSkills(@RequestBody CoordinatorDetails coordinatorDetails) throws Exception{
		ChatsApiResponse deleteCoordinatorApiResponse = new ChatsApiResponse();
		try {
		CoordinatorDetails coordinatorGroupAndRoomId =quickBloxService.getGroupId(coordinatorDetails.getTimebound_subject_config_id());
			coordinatorDetails.setGroupId(coordinatorGroupAndRoomId.getGroupId());
			coordinatorDetails.setRoomId(coordinatorGroupAndRoomId.getRoomId());
			System.out.println("CoordinatorDetails"+coordinatorDetails);
		CoordinatorDetails deletedCoordinator=	coordinatorChatsService.deleteCoordinator(coordinatorDetails);
			if(StringUtils.isBlank(deletedCoordinator.getGroupId())) {
				deleteCoordinatorApiResponse.setData(new CoordinatorDetails());
				deleteCoordinatorApiResponse.setMessage("Coordinator not deleted ! ");
				deleteCoordinatorApiResponse.setStatusCode(500);
				deleteCoordinatorApiResponse.setSuccess(false);
				return new ResponseEntity<ChatsApiResponse>(deleteCoordinatorApiResponse,HttpStatus.INTERNAL_SERVER_ERROR);
			}
			deleteCoordinatorApiResponse.setData(deletedCoordinator);
			deleteCoordinatorApiResponse.setMessage("Coordinator Deleted Sucessfully! ");
			deleteCoordinatorApiResponse.setStatusCode(200);
			deleteCoordinatorApiResponse.setSuccess(true);
			return new ResponseEntity<ChatsApiResponse>(deleteCoordinatorApiResponse,HttpStatus.OK);
		}
		catch(Exception e) {
			logger.info("Exception in adding coordiantor"+ ExceptionUtils.getFullStackTrace(e));
			deleteCoordinatorApiResponse.setData(new CoordinatorDetails());
			deleteCoordinatorApiResponse.setMessage(e.getMessage());
			deleteCoordinatorApiResponse.setStatusCode(500);
			deleteCoordinatorApiResponse.setSuccess(false);
			return new ResponseEntity<ChatsApiResponse>(deleteCoordinatorApiResponse,HttpStatus.INTERNAL_SERVER_ERROR);	
		}
		
	}
	@PostMapping("addCoordinatorAndSkills")
	public ResponseEntity<ChatsApiResponse> addCoordinatorAndSkills(@RequestBody CoordinatorDetails coordinatorDetails) throws Exception{
		ChatsApiResponse addedApiResponse = new ChatsApiResponse();
		try {
		CoordinatorDetails coordinatorGroupAndRoomId =quickBloxService.getGroupId(coordinatorDetails.getTimebound_subject_config_id());
	    coordinatorDetails.setGroupId(coordinatorGroupAndRoomId.getGroupId());
		coordinatorDetails.setRoomId(coordinatorGroupAndRoomId.getRoomId());
		System.out.println("CoordinatorDetails"+coordinatorDetails);
		CoordinatorDetails addedCoordinator= coordinatorChatsService.addCoordinator(coordinatorDetails);
		if(StringUtils.isBlank(addedCoordinator.getGroupId())) {
			addedApiResponse.setData(new CoordinatorDetails());
			addedApiResponse.setMessage("Coordinator not added ! ");
			addedApiResponse.setStatusCode(500);
			addedApiResponse.setSuccess(false);
			return new ResponseEntity<ChatsApiResponse>(addedApiResponse,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		addedApiResponse.setData(addedCoordinator);
		addedApiResponse.setMessage("Coordinator Added Sucessfully! ");
		addedApiResponse.setStatusCode(200);
		addedApiResponse.setSuccess(true);
		return new ResponseEntity<ChatsApiResponse>(addedApiResponse,HttpStatus.OK);

		}
		catch(Exception e) {
			logger.info("Exception in adding coordiantor"+ ExceptionUtils.getFullStackTrace(e));
			addedApiResponse.setData(new CoordinatorDetails());
			addedApiResponse.setMessage(e.getMessage());
			addedApiResponse.setStatusCode(500);
			addedApiResponse.setSuccess(false);
			return new ResponseEntity<ChatsApiResponse>(addedApiResponse,HttpStatus.INTERNAL_SERVER_ERROR);			
		}


}
}
